# openmw_transmuteCube

## Requires OpenMW 0.49 or above

## Youtube Demonstration
https://www.youtube.com/watch?v=SIKMFQtA-jk

## Installation
--todo

## To Use:
1. Bind quickkey 9 to open the dwemer cube
2. Bind quickkey 10 to activate the cube
3. Follow the recipe book
4. Enjoy! Or don't, I'm not your mother.

## TODO:
1. Add settings start with the cube or not (current behavior gives the player the cube immediately)
2. a)	Account for the cube being a quest item. (currently you can turn it in for the quest, and then its gone.)
2. b)	Or maybe the mod gives it back to you immediately? I haven't actually tested yet.
3. ~~If theres enough interest, I might use the new vfs file api to add user made recipes.~~	Done. (see recipes folder)

## Known Bugs:
None

## License
--TODO: say how it is licensed.

