local ui = require('openmw.ui')
local self = require('openmw.self')
local core = require('openmw.core')
local input = require('openmw.input')
local types = require('openmw.types')
local async = require('openmw.async')
local I = require('openmw.interfaces')
local aux_util = require('openmw_aux.util')
local storage = require('openmw.storage')


local cubeChest = nil


local playerSettings = storage.playerSection('Settings_transmuteCube_main_settings')
local startWithCube = playerSettings:get("StartWithCube")
local enableDefaultRecipes = playerSettings:get("EnableDefaultRecipes")


input.registerTrigger {
	l10n = "openmw_transmuteCube",
	name = "Open Transmutation Cube",
	key = "OpenCube",
	description = "desc_OpenTransmuteCube",
}


input.registerTrigger {
	l10n = "openmw_transmuteCube",
	name = "Activate Transmutation Cube",
	key = "ActivateCube",
	description = "desc_ActivateTransmuteCube",
}


input.registerTriggerHandler("ActivateCube", async:callback(function()
	core.sendGlobalEvent("event_transmute", {player = self, cubeChest = cubeChest})
end))

input.registerTriggerHandler("OpenCube", async:callback(function()
	if I.UI.getMode() == nil then
		I.UI.addMode("Container", {target = cubeChest})
	elseif I.UI.getMode():match("Container") then
		I.UI.removeMode("Container", {target = cubeChest})
	else
		I.UI.addMode("Container", {target = cubeChest})
	end
end))



local function onInit()
	core.sendGlobalEvent("event_requestCube", {player = self, startWithCube = startWithCube, enableDefaultRecipes = enableDefaultRecipes})
end

local function onLoad(saveData)
	cubeChest = saveData.cubeChest
end

local function onSave()
	return {cubeChest = cubeChest}
end

local function event_openCube()
	I.UI.addMode("Container", {target = cubeChest})
end

local function event_setCubeChest(cubeContainer)
	cubeChest = cubeContainer
end


return {
	engineHandlers = { 
		onInit = onInit,
		onLoad = onLoad, 
		onSave = onSave,
		onInputAction = onInputAction
	},
	eventHandlers = {
		event_notify = event_notify,
		e_tx_hadCube = e_tx_hadCube,
		event_openCube = event_openCube,
		event_setCubeChest = event_setCubeChest,
		e_rx_toggleOpenCube = e_rx_toggleOpenCube
	}
}

