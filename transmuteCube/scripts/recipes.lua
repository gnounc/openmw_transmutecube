local I = require('openmw.interfaces')
local types = require('openmw.types')
local storage = require('openmw.storage')
local aux_util = require('openmw_aux.util')
local async = require('openmw.async')


local function registerDefaultRecipes()

	I.gnounc_transmuteCube.registerRecipes({
		{product = "dwarven shortsword", ingredients = {{id = "ingred_scrap_metal_01", quantity = 1}, {id = "chargen dagger", quantity = 1}}, description = ""},
		{product = "glass arrow", ingredients = {{id_contains = " arrow$", itemType = types.Weapon, quantity = 50}, {id = "ingred_raw_glass_01", quantity = 1}}, description = "For Glass Arrows:<BR>    1 Stack of Arrows<BR>    1 shard of raw glass"},
		{product = "ebony arrow", ingredients = {{id_contains = " arrow$", itemType = types.Weapon, quantity = 50}, {id = "ingred_raw_ebony_01", quantity = 1}}, description = "For Ebony Arrows:<BR>    1 Stack of Arrows<BR>    1 piece of raw ebony"},
		{product = "steel arrow", ingredients = {{id_contains = " arrow$", itemType = types.Weapon, quantity = 50}, {id = "ingred_scrap_metal_01", quantity = 1}}, description = "For Iron Arrows:<BR>    1 Stack of Arrows<BR>     piece of scrap metal"},
		{product = "bonemold arrow", ingredients = {{id_contains = " arrow$", itemType = types.Weapon, quantity = 50}, {id = "ingred_bonemeal_01", quantity = 1}}, description = "For Bonemold Arrows:<BR>    1 Stack of Arrows<BR>    1 part bonemeal"},
		{product = "daedric arrow", ingredients = {{id_contains = " arrow$", itemType = types.Weapon, quantity = 50}, {id = "ingred_daedras_heart_01", quantity = 1}}, description = "For Daedric Arrows:<BR>    1 Stack or Halfstack of Arrows<BR>    1 Daedric Heart"},
		{product = {id = "ingred_ash_salts_01", quantity = 5}, ingredients = {{id = "misc_6th_ash_statue_01", quantity = 1}}, description = "For Ash Salts:<BR>    1 Ash Statue of the 6th House"},
		{product = {id = "ingred_bonemeal_01", quantity = 3}, ingredients = {{id = "misc_skull00", quantity = 1}}, description = "For Bonemeal:<BR    1 Skull"},
		{product = {upgrade = 1}, ingredients = {{itemType = types.Lockpick, quantity = 3}}, description = "Increase Lockpick Item Quality:<BR>    3 Lockpicks alike"},
		{product = {upgrade = 1}, ingredients = {{itemType = types.Repair, quantity = 3}}, description = "Increase Repair Item Quality:<BR>    3 tools alike"},
		{product = {upgrade = 1}, ingredients = {{itemType = types.Clothing, quantity = 3}}, description = "Increase Ring Quality:<BR>    3 Rings alike <BR><BR>Increase Amulet Quality:<BR>    3 Amulets alike <BR><BR>Increase Clothing Quality:<BR>    3 Articles of Clothing alike"},
		{product = {upgrade = 1}, ingredients = {{id_contains = "_soulgem_", itemType = types.Miscellaneous, quantity = 3}}, description = "Increase Soul Gem Quality:<BR>    3 Soul Gems of the same quality"},
		{product = {str_sub = {1, "_b$", "_c"}}, ingredients = {{itemType = types.Potion, quantity = 3}}, description = "Increase Potion Quality:<BR>    3 potions alike"},

		{product = "common_amulet", ingredients = {{id_contains = "common_ring", itemType = types.Clothing, quantity = 3}, {id = "ingred_gravedust_01", quantity = 1}}, description = "Common Ring:<BR>    3 Common amulets<BR>    1 portion gravedust"},
		{product = "expensive_amulet", ingredients = {{id_contains = "expensive_ring", itemType = types.Clothing, quantity = 3}, {id = "ingred_gravedust_01", quantity = 1}}, description = "Expensive Ring:<BR>    3 Expensive amulets<BR>    1 portion gravedust"},
		{product = "extravagant_amulet", ingredients = {{id_contains = "extravagant_ring", itemType = types.Clothing, quantity = 3}, {id = "ingred_gravedust_01", quantity = 1}}, description = "Extravagant Ring:<BR>    3 Extravagant amulets<BR>    1 part gravedust"},
		{product = "exquisite_amulet", ingredients = {{id_contains = "exquisite_ring", itemType = types.Clothing, quantity = 3}, {id = "ingred_gravedust_01", quantity = 1}}, description = "Exquisite Ring:<BR>    3 Exquisite amulets<BR>    1 portion gravedust"},

		{product = "common_ring", ingredients = {{id_contains = "common_amulet", itemType = types.Clothing, quantity = 3}, {id = "ingred_gravedust_01", quantity = 1}}, description = "Common Amulet:<BR>    3 Common Rings<BR>    1 portion gravedust"},
		{product = "expensive_ring", ingredients = {{id_contains = "expensive_amulet", itemType = types.Clothing, quantity = 3}, {id = "ingred_gravedust_01", quantity = 1}}, description = "Expensive Amulet:<BR>    3 Expensive Rings<BR>    1 portion gravedust"},
		{product = "extravagant_ring", ingredients = {{id_contains = "extravagant_amulet", itemType = types.Clothing, quantity = 3}, {id = "ingred_gravedust_01", quantity = 1}}, description = "Extravagant Amulet:<BR>    3 Extravagant Rings<BR>    1 portion gravedust"},
		{product = "exquisite_ring", ingredients = {{id_contains = "exquisite_amulet", itemType = types.Clothing, quantity = 3}, {id = "ingred_gravedust_01", quantity = 1}}, description = "Exquisite Amulet:<BR>    3 Exquisite Rings<BR>    1 portion gravedust"},
	})
end


return {
	interfaceName = "gnounc_transmuteCube_default_recipes",
	interface = {
		version = 1,
		registerDefaultRecipes = registerDefaultRecipes,
	},
}