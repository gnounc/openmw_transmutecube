local core = require('openmw.core')
local vfs = require('openmw.vfs')
local util = require('openmw.util')
local types = require('openmw.types')
local async = require('openmw.async')
local world = require('openmw.world')
local storage = require('openmw.storage')

local ItemUsage = require('openmw.interfaces').ItemUsage
local Activation = require('openmw.interfaces').Activation
local Controls = require('openmw.interfaces').Controls
local I = require('openmw.interfaces')
local aux_util = require('openmw_aux.util')


--affecting bug: https://gitlab.com/OpenMW/openmw/-/issues/7663


local recipes = {}
local recipe_book_contents = "<DIV ALIGN='LEFT'><FONT COLOR='000000' SIZE='3' FACE='Magic Cards'><BR>Unscovered Recipes of the Dwemer Transmologist<BR><BR>"


local globalSettings = storage.globalSection('Settings_transmuteCube_main_settings')


local debug = false 



local function d_print(fname, msg)
	if not debug then return end

	if fname == nil then
		fname = "\x1b[35mnil"
	end

	if msg == nil then
		msg = "\x1b[35mnil"
	end

	print("\n\t\x1b[33;3m" .. tostring(fname) .. "\n\t\t\x1b[33;3m" .. tostring(msg) .. "\n\x1b[39m")
end 

local function getCubeContents(cubeChest)
	return types.Container.inventory(cubeChest):getAll()
end

local function hasCube(player)
	local inv = types.Actor.inventory(player):getAll(types.Miscellaneous)

	for _, item in pairs(inv) do
		if item.recordId == "misc_dwrv_ark_cube00" then
			return true
		end
	end

	return false
end


local function craftRecipe(player, cubeChest, recordID, count)
	local inv = types.Container.inventory(cubeChest):getAll()

	--create new item and jam it into the cube
	--dont bork if item doesnt exist
	local success, result = pcall(function()
		return world.createObject(recordID, count)
	end)

	if success then
		d_print("created: ", recordID)
		newItem = result
	else
		d_print("failed to create item: ", recordID)
		return
	end

	--we've already checked that the stacksize and ingredients are correct. so just nuke the items.
	--and creating the item didnt fail.
	for _, item in pairs(inv) do
		item:remove()
	end

	newItem:moveInto(types.Container.inventory(cubeChest))
	core.sound.playSound3d("alteration cast", player)
 
	player:sendEvent("event_openCube", nil)
end

--rewrite these as tables i can index into, and go back 1/forward 1 from?
local function upgradeItem(item)

	d_print("upgradeItem", item.recordId)

	local product = item.recordId

	if types.Apparatus.objectIsInstance(item) then
		if product:match("pipe_01$") then
			return "apparatus_a_alembic_01"
		elseif product:match("_a_") then
			return string.gsub(product, "_a_", "_j_")
		elseif product:match("_j_") then
			return string.gsub(product, "_j_", "_m_")
		elseif product:match("_m_") then
			return string.gsub(product, "_m_", "_g_")
		elseif product:match("_g_") then
			return string.gsub(product, "_g_", "_sm_")
		end
	end

	if types.Armor.objectIsInstance(item) then
	end

	if types.Book.objectIsInstance(item) then
		if product == "sc_lesserdomination" then
			return "sc_greaterdomination"
		elseif product == "sc_greaterdomination" then
			return "sc_supremedomination"
		end
	end

	if types.Clothing.objectIsInstance(item) then
		if product:match("common") then
			return string.gsub(product, "common", "expensive")
		elseif product:match("expensive") then
			return string.gsub(product, "expensive", "extravagant")
		elseif product:match("extravagant") then
			return string.gsub(product, "extravagant", "exquisite")
		end
	end

	if types.Lockpick.objectIsInstance(item) then
		if product:match("apprentice") then
			return string.gsub(product, "apprentice", "journeyman")
		end
		if product:match("journeyman") then
			return "pick_master"
		end
		if product:match("_master") then
			return string.gsub(product, "_master", "_grandmaster")
		end
		if product:match("_grandmaster") then
			return string.gsub(product, "_grandmaster", "_secretmaster")
		end
	end

	if types.Miscellaneous.objectIsInstance(item) then
		if product:match("soulgem") then
			if product:match("petty") then
				return string.gsub(product, "petty", "lesser")
			elseif product:match("lesser") then
				return string.gsub(product, "lesser", "common")
			elseif product:match("common") then
				return string.gsub(product, "common", "greater")
			elseif product:match("greater") then
				return string.gsub(product, "greater", "grand")
			end
		end
	end

	if types.Potion.objectIsInstance(item) then
		if product:match("_b$") then
			return string.gsub(product, "_b$", "_c$")
		elseif product:match("_c$") then
			return string.gsub(product, "_c$", "_s$")
		elseif product:match("_s$") then
			return string.gsub(product, "_s$", "_q$")
		elseif product:match("_q$") then
			return string.gsub(product, "_q$", "_e$")
		end
	end


	if types.Probe.objectIsInstance(item) then
		if product:match("probe_bent") then
			return "probe_apprentice_01"
		end
		if product:match("probe_apprentice_01") then
			return "probe_journeyman_01"
		end
		if product:match("probe_journeyman_01") then
			return "probe_master"
		end
		if product:match("_master") then
			return string.gsub(product, "_master", "_grandmaster")
		end
		if product:match("_grandmaster") then
			return string.gsub(product, "_grandmaster", "_secretmaster")
		end
	end

	if types.Repair.objectIsInstance(item) then
		if product:match("repair_prongs") then
			return "hammer_repair"
		end
		if product:match("hammer_repair") then
			return "repair_journeyman_01"
		end
		if product:match("repair_journeyman_01") then
			return "repair_master_01"
		end
		if product:match("repair_master_01") then
			return "repair_grandmaster_01"
		end
		if product:match("repair_grandmaster_01") then
			return "repair_secretmaster_01"
		end
	end

	if types.Weapon.objectIsInstance(item) then
	end

	return nil
end


--TODO: add interface for adding headers and body to the recipe book
--TODO: add name and id to recipes, so we can check if user has them, override them, and give them a title/body
local function createRecipeBook()

	local bookTemplate = types.Book.record('bookskill_sneak5')
	local bookRecordTable = {name = "Uncovered Recipes of the Dwemer", template = bookTemplate, text = recipe_book_contents, skill = ""}
	local recordDraft = types.Book.createRecordDraft(bookRecordTable)
	local newBookRecord = world.createRecord(recordDraft)
	local newBook = world.createObject(newBookRecord.id)

	local recipeBookText = types.Book.records[newBook.recordId].text

	return newBook
end


--recipe validation
local function registerRecipes(arg_recipes)

	for _, recipe in pairs(arg_recipes) do
		assert(recipe.description ~= nil, "failed: description was nil")
		assert(recipe.ingredients ~= nil and next(recipe.ingredients) ~= nil, recipe.description .. " failed: ingredients list was empty")
		assert(recipe.product ~= nil and recipe.product ~= "", recipe.description .. " failed: product field was empty")
		assert(type(recipe.product) ~= "table" or next(recipe.product) ~= nil,  recipe.description .. " failed: recipe.product was invalid number")
		assert(recipe.product.upgrade == nil or recipe.ingredients[recipe.product.upgrade] ~= nil, recipe.description .. " failed:  invalid product.upgrade index")

		if recipe.product.str_sub ~= nil then
			--calling next multiple times. intentional
			assert(next(recipe.product.str_sub) ~= nil, recipe.description .. " failed: too few arguments to str_sub")
			assert(next(recipe.product.str_sub) ~= nil, recipe.description .. " failed: too few arguments to str_sub")
			assert(next(recipe.product.str_sub) ~= nil, recipe.description .. " failed: too few arguments to str_sub")
			assert(recipe.ingredients[recipe.product.str_sub[1]] ~= nil, recipe.description .. " failed:  invalid product.str_sub index")
		end

		for _, ingredient in pairs(recipe.ingredients) do
			assert(ingredient.quantity ~= nil, recipe.description .. " failed: ingredient.quantity was nil")
			assert(ingredient.quantity > 0, recipe.description .. " failed: ingredient.quantity was less than 1")
			assert(ingredient.id_contains ~= nil or ingredient.itemType ~= nil or (type(ingredient.id) == "string" and ingredient.id ~= "") or (type(ingredient.id) == "table" and (ingredient.id.id_contains ~= nil or ingredient.id.itemType )), "itemType, or ingredient.id was empty")
		end

		if recipe.description ~= "" then
			recipe_book_contents = recipe_book_contents .. "<DIV ALIGN='LEFT'>" .. recipe.description .. "<BR><BR>"
		end


		table.insert(recipes, recipe)
	end
end


local function transmuteCube(player, cubeChest)

	if hasCube(player) == false then return false end

	local inv = types.Container.inventory(cubeChest)
	local inv_contents = types.Container.inventory(cubeChest):getAll()

	if #inv_contents < 1 then return end --nothing in the cube, dont index out of boundts plz

	local item = nil

	--loop over recipes table, 
	for _, recipe in pairs(recipes) do

		--because ingredients list isnt a flat list, we cant use the # operator to get the length. we have to count it ourselves.
		local recipe_length = 0
		for idx, _ in pairs(recipe.ingredients) do
			recipe_length = recipe_length + 1
		end

		--discard any recipe where the ingredients list size does not match the cube container inventory size
		if recipe_length ~= #inv_contents then
			d_print("recipe legth mismatch.", "expected(" .. tostring(recipe_length) .. ") .. was (" .. tostring(#inv_contents) .. ")")
		else
			--assume 1 until specified
			local count = 1
			local itemId = nil

			if type(recipe.product) == "string" then
				itemId = recipe.product
			elseif type(recipe.product) == "table" then
				local ing = nil
				local idx = nil

				if recipe.product.quantity ~= nil then count = recipe.product.quantity end

				local int_upgrade = recipe.product.upgrade
				local t_sub = recipe.product.str_sub

				if int_upgrade ~= nil then
					idx = int_upgrade
				elseif t_sub ~= nil then
					idx = t_sub[1]
				end


				local potential_items = inv_contents

				if recipe.ingredients == nil then
					d_print("recipe.ingredients was ", nil)
				end

				if idx == nil then
					d_print("idx was ", nil)
				end

				if recipe.ingredients[idx] == nil then
					d_print("recipe.ingredients[idx] was ", nil)
				end


				if idx ~= nil then
					local item_type = recipe.ingredients[idx].itemType
					if item_type ~= nil then
						potential_items = inv:getAll(item_type)
					end

					local str_contains = recipe.ingredients[idx].id_contains
					if str_contains ~= nil then
						potential_items = aux_util.mapFilter(potential_items, function(p_item) return p_item.recordId:match(str_contains) ~= nil end)
					end
				end


				--can be nil
				item = potential_items[1]
				if item == nil then 
					d_print("no type or pattern match", "next recipe.")  --item type and pattern match failed to find any candidates. ingredient list does not make a recipe. bail.
					goto continue_recipe
				end

				itemId = item.recordId

				if recipe.product.id ~= nil then	--TODO: !!!can check this earlier, and skip all the substitution nonsense. fix this later
					itemId = recipe.product.id
				elseif int_upgrade ~= nil then
					itemId = upgradeItem(item)
				elseif t_sub ~= nil then
					if itemId:match(t_sub[2]) then
						itemId = string.gsub(itemId, t_sub[2], t_sub[3])
					else
						d_print("no match found for str_sub: ", t_sub[2])
					end
				end

				d_print("new item id", itemId)
			end

--by now we know which product we are making.
--so see which ingredients we are using, and find out if they are sufficient below

			local tmp = {}
			--need to pre-search for types
			for _, ingredient in pairs(recipe.ingredients) do

				if ingredient.id ~= nil then

					local ing_found = inv:find(ingredient.id)
					if ing_found == nil then
						d_print("transmute failed ingredient not found: ",  ingredient.id)
						goto continue_ingredient
					end

					if ing_found.count ~= ingredient.quantity then
						d_print("transmute failed: ingregdient in incorrect quantity", "Was (" .. ing_found.count .. "), expected (" .. ingredient.quantity .. ")")
						goto continue_ingredient
					end

					--we are adding potential ingredients in a loop. dont double-count them
					if tmp[ing_found.recordId] == nil then
						d_print("5.5 ingredient of correct quantity found", "adding ingredient " .. ing_found.recordId .. " to table")
						tmp[ing_found.recordId] = true	--table.insert results in double inserts. so do this instead
					end
				end
				

				local ing_item_type = ingredient.itemType					
				local ing_potential_items = inv_contents

				if ing_item_type ~= nil then
					ing_potential_items = inv:getAll(ing_item_type)
					if #ing_potential_items < 1 then
						d_print("transmute failed: ingredient error", "itemType not found")
						goto continue_ingredient
					end
				end

				local ing_contains = ingredient.id_contains
				if ing_contains ~= nil then
					ing_potential_items = aux_util.mapFilter(ing_potential_items, function(p_item) return tmp[p_item.recordId] == nil and string.lower(p_item.recordId):match(ing_contains) ~= nil end)
				end

				if #ing_potential_items < 1 then 
					d_print("transmute failed: itemType not found: ", tostring(ing_item_type))
					goto continue_ingredient
				end

				for _, itm in pairs(ing_potential_items) do
					if itm.count == ingredient.quantity then

						if tmp[itm.recordId] == nil then
							d_print("7.5 ingredient of correct quantity found", "adding ingredient " .. itm.recordId .. " to table")
							tmp[itm.recordId] = true	--table.insert results in double inserts. so do this instead
						end

						goto continue_ingredient
					end
				end

				::continue_ingredient::
			end

			--we are adding values to tmp to prevent double insertion. because of this, it is not a list, so we cannot use the # operator to get its length
			local tmp_count = 0
			for _, _ in pairs(tmp) do
				tmp_count = tmp_count + 1
			end

			--if all ingredients match, then temp table will match ingredients table. and we can fire off a craft recipe
			if #inv_contents == tmp_count then 
				if itemId ~= nil then
					d_print("crafting recipe:", itemId)
					craftRecipe(player, cubeChest, itemId, count)
				else
					d_print("itemId was nil.", "")
				end
			else
				d_print("skipping recipe, cube and tmp content lengths not equal", "expected (" .. tostring(#inv_contents) .. ") but was (" .. tostring(#tmp) .. ")")
			end
		end
	
		::continue_recipe::
	end
end

local function event_transmute(data)
	transmuteCube(data.player, data.cubeChest)
end

local function event_requestCube(data)

	local theCube = world.createObject("misc_dwrv_ark_cube00", 1)
	local cubeChest = world.createObject("dwemer_cube", 1)

	if data.enableDefaultRecipes == true then
		I.gnounc_transmuteCube_default_recipes.registerDefaultRecipes()
	end

	local cellItems = world.getCellByName("toddtest"):getAll(types.Container)
	cubeChest:teleport("toddtest", util.vector3(2035.569580078125, 3245.40771484375, -152.9093017578125))


--allow the cubeChest time to teleport.
	async:newUnsavableSimulationTimer(0.1, function() 

		if data.startWithCube == true then 
			theCube:moveInto(types.Actor.inventory(data.player))
		end

		local recipeBook = createRecipeBook()
		recipeBook:moveInto(types.Container.inventory(cubeChest))
	
		--give the player script a reference to the chest
		data.player:sendEvent("event_setCubeChest", cubeChest)
	end)

end

local function onInit()
	ItemUsage.addHandlerForType(types.Miscellaneous, function(item, player)
		if item.recordId == "misc_dwrv_ark_cube00" then
			player:sendEvent("event_openCube", nil)
			return false
		end
	end)
end


return { 
	interfaceName = "gnounc_transmuteCube",
	interface = {
		version = 1,
		registerRecipes = registerRecipes,

		hasCube = hasCube,
		craftRecipe = craftRecipe,
		getCubeContents = getCubeContents,
		transmutecube = transmutecube
	},
	engineHandlers = {
		onInit = onInit
	},
	eventHandlers = {
		event_transmute = event_transmute,
		event_requestCube = event_requestCube,
		e_tx_openCube = e_tx_openCube,
	}
}

