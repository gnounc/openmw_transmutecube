local input = require('openmw.input')
local interfaces = require('openmw.interfaces')



interfaces.Settings.registerPage {
	l10n = "openmw_transmuteCube",
	name = "Transmutation Cube Settings",
	key = "transmuteCube_settings",
	description = "desc_transmuteCube_settings",
}


interfaces.Settings.registerGroup {
	l10n = "openmw_transmuteCube",
	name = "Main Settings",
	key = "Settings_transmuteCube_main_settings",
	description = "underline_main_settings",

	page = "transmuteCube_settings",
	permanentStorage = true,
	settings = {
		{
			name = "Start with Cube in Inventory",
			key = "StartWithCube",
			description = "desc_startWithCube",
			renderer = "checkbox",
			default = true,
		},
		{
			name = "Enable Default Recipes",
			key = "EnableDefaultRecipes",
			description = "desc_enableDefaultRecipes",
			renderer = "checkbox",
			default = true,
		},
	},
}


interfaces.Settings.registerGroup {
	l10n = "openmw_transmuteCube",
	name = "Key Bindings",
	key = "Settings_transmuteCube_keybinds",
	description = "underline_keybinds",

	page = "transmuteCube_settings",
	permanentStorage = true,
	settings = {
		{
			name = "Open Transmutation Cube",
			key = "OpenCube",
			description = "desc_OpenTransmuteCube",
			renderer = "inputBinding",
			default = "L",
			argument = {
				type = "trigger",
				key = "OpenCube",
			}
		},
		{
			name = "Activate Transmutation Cube",
			key = "ActivateCube",
			description = "desc_ActivateTransmuteCube",
			renderer = "inputBinding",
			default = "U",
			argument = {
				type = "trigger",
				key = "ActivateCube",
			}
		}
	},
}

